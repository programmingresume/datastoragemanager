package network.marble.datastoragemanager;

import lombok.Getter;
import network.marble.datastoragemanager.config.Settings;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.ResourceManager;
import network.marble.datastoragemanager.database.models.*;
import network.marble.datastoragemanager.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class DataStorageManager extends JavaPlugin {

	@Getter private static DataStorageManager instance;
	@Getter private static ResourceManager resourceManager;
	@Getter private static DAOManager daoManager;
	@Getter private static Settings settings;

	public void onEnable() {
		instance = this;
		settings = retrieveConfig();
		
		if (settings == null){
			createConfig();
			settings = retrieveConfig();
		}
		
		if (settings == null){
			this.setEnabled(false);
		}

		resourceManager = new ResourceManager();
		daoManager = new DAOManager(resourceManager);
		
		registerListeners();
	}

	private void registerListeners() {
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	private Settings retrieveConfig(){
		try(Reader reader = new FileReader("plugins/MRN-DataStorageManager/config.json")){
			Settings container = new Gson().fromJson(reader, Settings.class);
			
			return container;
		} catch (Exception e){
			getLogger().info("Config does not exist... Creating");
		}
		return null;
	}

	private void createConfig(){
		Settings builder = new Settings(); 
		File directory = new File("plugins/MRN-DataStorageManager");
		
		if (directory.exists() == false){
			getLogger().info("Creating Directory");
			directory.mkdir();
			getLogger().info("Created");
		}
		
		try(Writer writer = new BufferedWriter( new OutputStreamWriter( new FileOutputStream("plugins/MRN-DataStorageManager/config.json" )))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			this.setEnabled(false);
		}
	}

	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> list = new ArrayList<>();
		
		list.add(Minigame.class);
		list.add(NetworkSettings.class);
		list.add(NetworkUser.class);
		list.add(NetworkUserPurchase.class);
		list.add(Rank.class);
		list.add(ModerationLog.class);
		list.add(BugReports.class);
		list.add(HubSettings.class);

		return list;
	}

}
