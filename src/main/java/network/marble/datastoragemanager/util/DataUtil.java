package network.marble.datastoragemanager.util;

import java.util.ArrayList;
import java.util.List;

import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.NetworkUserHubData;
import network.marble.datastoragemanager.database.models.RankPermission;
import network.marble.datastoragemanager.database.models.Rank;

public class DataUtil {

    public static void updateNetworkUser(NetworkUser user, String name, String ip) {
        if (user.getName() == null || user.getName().equalsIgnoreCase(name) == false) {
            user.setName(name);
        }

        if (user.getIp() == null || user.getIp().equalsIgnoreCase(ip) == false) {
            user.setIp(ip);
        }

        if (user.getNetworkUserHubData() == null) {
            user.setNetworkUserHubData(new NetworkUserHubData());
        }

        DAOManager.getNetworkUserDAO().save(user);
    }
    
    public static void updateRank(Rank rank, String name, String displayName){
    	if (rank.getName() == null || rank.getName().equalsIgnoreCase(name) == false) {
            rank.setName(name);
        }

        if (rank.getDisplayName() == null || rank.getDisplayName().equalsIgnoreCase(displayName) == false){
			rank.setDisplayName(displayName);
		}
        
        List<RankPermission> nodes = new ArrayList<RankPermission>();
        rank.setRankPermission(nodes);

        DAOManager.getRankDAO().save(rank);
    }

    public static void validate(NetworkUser user) {
        if (user.getRank() == null) {
            user.setRank("DEFAULT");
        }
    }
}
