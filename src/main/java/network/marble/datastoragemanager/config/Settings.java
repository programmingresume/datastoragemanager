package network.marble.datastoragemanager.config;

public class Settings {
    public String host = "127.0.0.1";
    public int port = 27017;
    public String username = "";
    public String password = "";
    public String database = "marblenetworkcore";
    
    public Settings(){
    	
    }
}
