package network.marble.datastoragemanager.database;

import lombok.Getter;
import network.marble.datastoragemanager.DataStorageManager;
import network.marble.datastoragemanager.database.dao.*;
import network.marble.datastoragemanager.database.models.*;

import org.mongodb.morphia.Datastore;

import java.util.HashSet;

public class DAOManager {

    @Getter private static Datastore datastore;

    @Getter private static NetworkSettingsDAO networkSettingsDAO;
    @Getter private static NetworkUserDAO networkUserDAO;
    @Getter private static NetworkUserPurchaseDAO networkUserPurchaseDAO;
    @Getter private static RankDAO rankDAO;
    @Getter private static MinigameDAO minigameDAO;
    @Getter private static ModerationLogDAO moderationLogDAO;
    @Getter private static BugReportDAO bugReportDAO;
    @Getter private static HubSettingsDAO hubSettingsDAO;

    public DAOManager(ResourceManager resourceManager) {
        datastore = resourceManager.getResource().getDatastore(new HashSet<Class>(DataStorageManager.getInstance().getDatabaseClasses()));
        ensureIndexes(datastore);
        registerDAO(datastore);
    }

    private void registerDAO(Datastore datastore) {
        networkSettingsDAO = new NetworkSettingsDAO(datastore);
        networkUserDAO = new NetworkUserDAO(datastore);
        networkUserPurchaseDAO = new NetworkUserPurchaseDAO(datastore);
        rankDAO = new RankDAO(datastore);
        minigameDAO = new MinigameDAO(datastore);
        moderationLogDAO = new ModerationLogDAO(datastore);
        bugReportDAO = new BugReportDAO(datastore);
        hubSettingsDAO = new HubSettingsDAO(datastore);
    }

    private void ensureIndexes(Datastore datastore) {
        datastore.ensureIndexes(NetworkSettings.class);
        datastore.ensureIndexes(NetworkUser.class);
        datastore.ensureIndexes(NetworkUserPurchase.class);
        datastore.ensureIndexes(Rank.class);
        datastore.ensureIndexes(Minigame.class);
        datastore.ensureIndexes(ModerationLog.class);
        datastore.ensureIndexes(BugReports.class);
        datastore.ensureIndexes(HubSettings.class);
    }
}
