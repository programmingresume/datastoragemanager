package network.marble.datastoragemanager.database.quickmongo;


public class DatabaseObject {
	String host = "";
	int port = 0;
	String username = "";
	String password = "";
	String database = "";

	public DatabaseObject(String host, int port)
	{
		this.host = host;
		this.port = port;
	}

	public void withUsername(String username)
	{
		this.username = username;
	}

	public void withPassword(String password)
	{
		this.password = password;
	}

	public void withDatabase(String database)
	{
		this.database = database;
	}
}
