package network.marble.datastoragemanager.database.quickmongo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import network.marble.datastoragemanager.DataStorageManager;
import network.marble.datastoragemanager.config.Settings;

public class MongoResource {
	
	DatabaseObject db;
	Logger logger;
	Settings settings = DataStorageManager.getSettings();
	
	private MongoClient mongo = null;
	
	public MongoResource(DatabaseObject db, Logger logger){
		this.db = db;
		this.logger = logger;
	}

	public Datastore getDatastore(HashSet<Class> classes){
		
		MongoClient mongoClient = new MongoClient(); //Debug
		
		final Morphia morphia = new Morphia();
		final Datastore datastore = morphia.createDatastore(mongoClient, settings.database);
		morphia.map(classes);
		datastore.ensureIndexes();
		
		return datastore;
	}
	
	public MongoClient getMongo(){
		if(mongo == null){
			MongoCredential credentials = MongoCredential.createMongoCRCredential(settings.username, settings.database, settings.password.toCharArray());
			mongo = new MongoClient(new ServerAddress(settings.host, settings.port), Arrays.asList(credentials));
		}
		return mongo;
	}
}
