package network.marble.datastoragemanager.database.models;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Serialized;
import org.mongodb.morphia.utils.IndexDirection;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import network.marble.datastoragemanager.api.DataAPI;

@Entity(value = "ranks", noClassnameStored = true)
@NoArgsConstructor
public class Rank {

    public Rank(String name) {
        this.name = name;
        this.displayName = "";
        this.inherits = "";
        this.isPriority = false;
        this.rankPermission = new ArrayList<>();
    }
	
	@Id
	@Getter
	private ObjectId id;
	
	@Indexed(value = IndexDirection.ASC, name = "name", unique = true)
	@Getter @Setter
	private String name;

	@Getter @Setter
	private String displayName;
	
	@Getter @Setter
	private String inherits;
	
	@Getter @Setter
	private Boolean isPriority;
	
	@Getter @Setter
	@Serialized
	private List<RankPermission> rankPermission;
	
	public void save(){
		DataAPI.saveRank(this);
	}
}
