package network.marble.datastoragemanager.database.models;

import org.bson.types.ObjectId;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(value = "bugreports", noClassnameStored = true)
public class BugReports {
	
	public BugReports(Player submitter, String reason){
		this.submitter = submitter.getUniqueId().toString();
		this.reason = reason;
		
		created = System.currentTimeMillis() % 1000;
	}
	
	@Id
	@Getter
	private ObjectId id;
	
	@Getter @Setter
	private String submitter;
	
	@Getter @Setter
	private String reason;
	
	@Getter @Setter
	private Long created;
}
