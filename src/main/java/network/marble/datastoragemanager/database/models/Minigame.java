package network.marble.datastoragemanager.database.models;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(value = "minigames", noClassnameStored = true)
public class Minigame {
	
	@Id
	@Getter
	private Long id;
	
	@Getter @Setter
	private String gameName;
	
	@Getter @Setter
	private Boolean isLive;
}
