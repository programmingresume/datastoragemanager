package network.marble.datastoragemanager.database.models;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Reference;

import lombok.Getter;
import lombok.Setter;

public class RankPermission implements Serializable {

	public RankPermission(){ this.node = null; }
	public RankPermission(String node) { this.node = node; }
	
	@Reference
	@Getter @Setter
	private String node;
	
}