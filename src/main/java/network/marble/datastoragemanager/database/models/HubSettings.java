package network.marble.datastoragemanager.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "hubsettings", noClassnameStored = true)
@NoArgsConstructor
public class HubSettings {

    public HubSettings(boolean weather) {
        this.weather = weather;
        this.identify = "Settings";
    }

    @Id
    @Getter
    private ObjectId id;

    @Getter
    private String identify;

    @Getter
    private boolean weather;
}
