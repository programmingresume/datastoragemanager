package network.marble.datastoragemanager.database.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;

import lombok.Getter;

public class ModerationLog {

	public ModerationLog(String actionTakenBy, String actionAgainst, Long time, String type, String reason, Long duration){
		this.actionTakenBy = actionTakenBy;
		this.actionAgainst = actionAgainst;
		this.time = time;
		this.type = type;
		this.reason = reason;
		this.duration = duration;
	}
	
	@Id
    @Getter
    private ObjectId id;
	
	@Getter
	private String actionTakenBy;
	
	@Getter
	private String actionAgainst;
	
	@Getter
	private Long time;
	
	@Getter
	private String type;
	
	@Getter
	private String reason;
	
	@Getter
	private Long duration;
	
}
