package network.marble.datastoragemanager.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import network.marble.datastoragemanager.util.SerializableVector;

import org.mongodb.morphia.annotations.Serialized;

@NoArgsConstructor
public class NetworkUserHubData {

    @Getter @Setter
    @Serialized
    private SerializableVector parkourCheckpoint;

}
