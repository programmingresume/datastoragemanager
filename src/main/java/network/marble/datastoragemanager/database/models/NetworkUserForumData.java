package network.marble.datastoragemanager.database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NetworkUserForumData {

	@Getter
	private int forumID;
	
	@Getter
	private boolean registered;
	
}
