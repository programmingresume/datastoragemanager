package network.marble.datastoragemanager.database;

import lombok.Getter;
import network.marble.datastoragemanager.DataStorageManager;
import network.marble.datastoragemanager.config.Settings;
import network.marble.datastoragemanager.database.quickmongo.DatabaseObject;
import network.marble.datastoragemanager.database.quickmongo.MongoResource;

public class ResourceManager {
	
    @Getter private MongoResource resource;

    public ResourceManager() {
        if (resource == null) {
            resource = new MongoResource(getDatabaseObject(), DataStorageManager.getInstance().getLogger());
        }
    }

    private DatabaseObject getDatabaseObject() {
        Settings settings = DataStorageManager.getSettings();

        DatabaseObject databaseObject = new DatabaseObject(settings.host, settings.port);

        if (settings.username.equals("") == false && settings.password.equals("") == false) {
            databaseObject.withUsername(settings.username);
            databaseObject.withPassword(settings.password);
        }

        if (settings.database.equals("") == false) {
            databaseObject.withDatabase(settings.database);
        }

        return databaseObject;
    }
	
}
