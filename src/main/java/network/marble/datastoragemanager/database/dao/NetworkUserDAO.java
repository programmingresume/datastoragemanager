package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.NetworkUser;

public class NetworkUserDAO extends BasicDAO<NetworkUser, ObjectId>{

    public NetworkUserDAO(Datastore ds) {
        super(ds);
    }

}
