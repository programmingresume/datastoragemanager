package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.Rank;

public class RankDAO extends BasicDAO<Rank, ObjectId> {
	
    public RankDAO(Datastore ds) {
        super(ds);
    }
}
