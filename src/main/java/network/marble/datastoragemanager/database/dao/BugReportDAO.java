package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import lombok.Getter;
import network.marble.datastoragemanager.database.models.BugReports;

public class BugReportDAO extends BasicDAO<BugReports, ObjectId> {

	@Getter private static BugReportDAO instance;
	
	public BugReportDAO(Datastore ds) {
        super(ds);
        instance = this;
	}
}
