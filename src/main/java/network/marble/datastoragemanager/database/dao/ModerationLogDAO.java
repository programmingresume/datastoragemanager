package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.ModerationLog;

public class ModerationLogDAO extends BasicDAO<ModerationLog, ObjectId> {
	
    public ModerationLogDAO(Datastore ds) {
        super(ds);
    }
}
