package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.NetworkSettings;

public class NetworkSettingsDAO extends BasicDAO<NetworkSettings, ObjectId> {

    public NetworkSettingsDAO(Datastore ds) {
        super(ds);
    }

}
