package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.NetworkUserPurchase;

public class NetworkUserPurchaseDAO extends BasicDAO<NetworkUserPurchase, ObjectId> {

    public NetworkUserPurchaseDAO(Datastore ds) {
        super(ds);
    }

}
