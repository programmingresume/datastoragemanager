package network.marble.datastoragemanager.database.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import network.marble.datastoragemanager.database.models.Minigame;

public class MinigameDAO extends BasicDAO<Minigame, ObjectId> {
	
    public MinigameDAO(Datastore ds) {
        super(ds);
    }
}
