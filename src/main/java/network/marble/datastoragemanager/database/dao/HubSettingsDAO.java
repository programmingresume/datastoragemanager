package network.marble.datastoragemanager.database.dao;

import network.marble.datastoragemanager.database.models.HubSettings;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class HubSettingsDAO extends BasicDAO<HubSettings, ObjectId> {
    public HubSettingsDAO(Datastore ds) {
        super(ds);
    }
}