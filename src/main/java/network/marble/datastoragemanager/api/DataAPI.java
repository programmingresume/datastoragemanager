package network.marble.datastoragemanager.api;

import network.marble.datastoragemanager.DataStorageManager;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.Minigame;
import network.marble.datastoragemanager.database.models.NetworkSettings;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.NetworkUserPurchase;
import network.marble.datastoragemanager.database.models.RankPermission;
import network.marble.datastoragemanager.database.models.Rank;
import network.marble.datastoragemanager.util.DataUtil;

import java.util.Collection;
import java.util.List;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.QueryResults;

public class DataAPI {

	/**
	 * Retrieve a network user instance from the users map if it exists,
	 * otherwise retrieve the user instance from the database.
	 *
	 * @param uuid the uuid of the user
	 *
	 * @return NetworkUser instance
	 */
	public static NetworkUser retrieveUser(String uuid) {
		NetworkUser user = DAOManager.getNetworkUserDAO().findOne("uuid", uuid);

		if (user == null) {
			return null;
		}

		return user;
	}

	/**
	 * Retrieve a network user instance from the users map by name if it exists,
	 * otherwise retrieve the user instance from the database.
	 *
	 * @param name the name of the user
	 * @return NetworkUser instance
	 */
	@Deprecated
	public static NetworkUser retrieveUserByName(String name) {
		for (NetworkUser user : DAOManager.getNetworkUserDAO().createQuery().field("name").containsIgnoreCase(name)) {
			if (user.getName().equalsIgnoreCase(name)) {
				return user;
			}
		}
		
		return null;
	}

	/**
	 * Initializes a network user instance for an online player.
	 *
	 * @param name the name of the user
	 * @param uuid the uuid of the user
	 * @param ip the ip of the user
	 * @return Networkuser instance
	 */
	public static NetworkUser initUser(String uuid, String name, String ip) {
		NetworkUser user = DAOManager.getNetworkUserDAO().findOne("uuid", uuid);
		if (user == null) {
			user = new NetworkUser(uuid);
		}	

		DataUtil.updateNetworkUser(user, name, ip);
		return user;
	}

	/**
	 * Get a user with the specified uuid
	 *
	 * @param uuid the uuid of the user
	 *
	 * @return the network user ebean mapped to the specified uuid
	 */
	public static NetworkUser getUser(String uuid) {
		return retrieveUser(uuid);
	}

	/**
	 * Save a users data. If a bean is dirty (contains changes) or is new
	 * it will be saved to the database.
	 *
	 * @param user the network user ebean of the user
	 */
	public static void saveUser(NetworkUser user) {
		DAOManager.getNetworkUserDAO().save(user);
	}
	
	/**
	 * Save a ranks data. If changes have been made to the rank it will be saved to the database.
	 * 
	 * @param rank the rank to be saved
	 */
	public static void saveRank(Rank rank){
		DAOManager.getRankDAO().save(rank);
	}


	/**
	 * Save a collection of users. If a bean is dirty (contains changes) or is new
	 * it will be saved to the database.
	 *
	 * @param users a collection of network users
	 */
	public static void saveMany(Collection<NetworkUser> users) {
		for (NetworkUser user : users) {
			DAOManager.getNetworkUserDAO().save(user);
		}
	}

	/**
	 * Create a transaction and optionally save the purchase immediately.
	 *
	 * @param user the ebean of the user
	 * @param type the type (or category) of the transaction
	 * @param data the data of the purchase as a string
	 * @param save whether the purchase should be saved now or later
	 */
	public static void createTransaction(NetworkUser user, String type, String data, boolean save) {
		NetworkUserPurchase purchase = new NetworkUserPurchase(type, data);
		DAOManager.getNetworkUserPurchaseDAO().save(purchase);

		if (save) {
			user.getNetworkUserPurchases().add(purchase);
			DAOManager.getNetworkUserDAO().save(user);
		}
	}

	/**
	 * Get rank
	 * 
	 * @param the name of the rank that will be retrieved 
	 * @return rank rank data
	 */
	public static Rank getNetworkRank(String name){
		Rank rank = DAOManager.getRankDAO().findOne("name", name);

		if (rank == null){
			return null;
		}

		return rank;
	}
	/**
	 * Create Rank
	 * 
	 * @param name the name of the rank to be created
	 * @param displayName the display name of the rank to be created
	 * @return
	 */
	public static Rank initRank(String name, String displayName){
		Rank rank = DAOManager.getRankDAO().findOne("name", name);
		if (rank == null) {
			rank = new Rank(name);
		}	

		DataUtil.updateRank(rank, name, displayName);
		return rank;
	}

	/**
	 * Check if rank is priority from string
	 * 
	 * @param rank the name of the rank to be checked
	 * @return whether the rank is priority
	 */
	public static Boolean rankIsPriority(String rank){
		Rank temp = DAOManager.getRankDAO().findOne("name", rank);

		if (temp.getIsPriority()){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Check if rank is priority from string
	 * 
	 * @param rank the rank to be checked
	 * @return whether the rank is priority
	 */
	public static Boolean rankIsPriority(Rank rank){

		if (rank.getIsPriority()){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Get the network settings.
	 *
	 * @return settings network settings
	 */
	public static NetworkSettings getNetworkSettings() {
		NetworkSettings settings = DAOManager.getNetworkSettingsDAO().find().get();

		if (settings == null) {
			settings = new NetworkSettings();
			saveNetworkSettings(settings);
		}

		return settings;
	}

	/**
	 * Get if the rank is priority
	 * 
	 * @param rank name of the rank
	 * @return if the user is priority for queue or not
	 */
	public static Boolean isPriority(String rank){
		Rank temp = DAOManager.getRankDAO().findOne("name", rank);

		if(temp.getIsPriority()){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Get if the user is priority
	 * 
	 * @param rank name of the rank
	 * @return if the user is priority for queue or not
	 */
	public static Boolean isPriority(Rank rank){

		if(rank.getIsPriority()){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Save the network settings.
	 */
	public static void saveNetworkSettings(NetworkSettings settings) {
		DAOManager.getNetworkSettingsDAO().save(settings);
	}

	/**
	 * @param user to check
	 * @param node the permission node to compare
	 * 
	 * @return user has/doesn't have permission
	 */
	@Deprecated
	public static Boolean hasPermission(NetworkUser user, String node){
		Rank rank = DAOManager.getRankDAO().findOne("name", user.getRank());
		List<RankPermission> perms = getPermissions(rank);
		DataStorageManager.getInstance().getLogger().info(rank.getName() + " " + user.getName());
		
		if(perms != null){
			for(RankPermission p : perms){
				DataStorageManager.getInstance().getLogger().info(p.getNode().toString());
				if(p.getNode().equals(node)){
					return true;
				}
				if(p.getNode().equals("*")){
					return true;
				}
			}
			DataStorageManager.getInstance().getLogger().info("Dropped out of for");
		} else {
			DataStorageManager.getInstance().getLogger().info("List Null");
		}
		return false;
	}

	/**
	 * Get a list of permissions associated with the given rank
	 * 
	 * @param rank to retrieve permissions of
	 * @return list of permissions
	 */
	public static List<RankPermission> getPermissions(Rank rank){
		Boolean hasInheritance = false;
		List<RankPermission> inheritsPerms = rank.getRankPermission();

		for (RankPermission r : rank.getRankPermission()){
			DataStorageManager.getInstance().getLogger().info(r.getNode().toString());
		}
		
		if (rank.getInherits() != "" || rank.getInherits() != null){
			hasInheritance = true;
			return inheritsPerms;
		}
		
		while(hasInheritance){
			rank = DAOManager.getRankDAO().findOne("name", rank.getInherits());
			inheritsPerms.addAll(rank.getRankPermission());
			if(rank.getInherits() == null){
				hasInheritance = false;
				return inheritsPerms;
			}
		}
		return inheritsPerms;
	}

	/**
	 * Returns int array containing the unique identifiers of all minigames that are currently live to the public
	 * 
	 * @return live minigame ids
	 */
	public static int[] minigameIDs(){
		int[] items = { };
		int i = 0;
		
		Query<Minigame> q = DAOManager.getMinigameDAO().createQuery().filter("isLive", true);
		QueryResults<Minigame> games = DAOManager.getMinigameDAO().find(q);
		
		for(Minigame g : games){
			items[i] = Math.toIntExact(g.getId());
			i++;
		}
		return items;
	}
}
