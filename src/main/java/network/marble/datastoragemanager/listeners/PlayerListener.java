package network.marble.datastoragemanager.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

import network.marble.datastoragemanager.DataStorageManager;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.models.NetworkUser;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public static void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        Long start = System.currentTimeMillis();

        NetworkUser user = DataAPI.initUser(event.getUniqueId().toString(), event.getName(), event.getAddress().getHostAddress());

        DataStorageManager.getInstance().getLogger().info("Processing Time: " + (System.currentTimeMillis() - start) + "ms");
    }
}
